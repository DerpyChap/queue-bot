# Queue Bot
Queue Bot is a bot that allows administrators to, well, add users to a queue that's stored in a channel. Queue Bot is based on the custom [Liara-Docker](https://gitlab.com/nerd3-servers/Liara-Docker) framework developed by myself (DerpyChap) and Jack Baron.

## Requirements
 - [Docker](https://docs.docker.com/install/)
    - Install instructions available for:
        - [Windows](https://docs.docker.com/docker-for-windows/install/)
        - [macOS](https://docs.docker.com/docker-for-mac/install/)
        - And Linux ([Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/), [Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/), [Debian](https://docs.docker.com/install/linux/docker-ce/debian/), [CentOS](https://docs.docker.com/install/linux/docker-ce/centos/))
 - [Docker Compose](https://docs.docker.com/compose/install/#install-compose)
    - Comes preinstalled with Docker for Windows and Docker for Mac

## Setting Up
***These instructions are based on Linux commands, but should (mostly) work in macOS' CLI and Windows' PowerShell***

Firstly, download the repository:
```
git clone https://gitlab.com/DerpyChap/queue-bot
```

After that, CD into the queue-bot folder and update the docker image:
```
cd queue-bot
docker-compose pull
```

Then, copy `example.env`, rename the copy to `bot.env`, open up the file, insert your bot token and save the file (You can also change the bot name if you wish):
```
cp example.env bot.env
nano bot.env
```

After that, you should be ready to go! You can run the bot in detached mode using:
```
docker-compose up -d
```

Once it's booted, you'll need to find out what prefix it's generated. To view this, and all of the bot's logs, run:
```
docker-compose logs -f
```

In Discord, you can run the `[p]set prefix` command to change your prefix. For example:
```
[p]set prefix q!
```
Where `[p]` is the current prefix and `q!` is the prefix of your choice.

### Loading the Cogs
By default, Liara doesn't load up any extra cogs. To load them up, go to Discord run the following commands:
```
[p]load cogs.bot.manage
[p]loadall
```
This should load up all of the bot's cogs. To view all the commands, run `[p]help`.

## Maintainers
- DerpyChap (<holla@derpychap.co.uk>)
