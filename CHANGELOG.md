# Changelog
This is the changelog. It lists changes made. I'm not sure what else to put here.

## 15th May 2019
### Added
 - This changelog file :D
 - Updated to the latest version of [Liara-Docker](https://gitlab.com/nerd3-servers/liara-docker), which includes a bunch of internal changes
     - These changes require a new config variable being added to the `bot.env` file called `CONFIG`, which should just be "config" by default. See the [`example.env`](https://gitlab.com/DerpyChap/queue-bot/blob/master/example.env) for an example
     - For more information on what the config variable and folder are for, see [here](https://gitlab.com/nerd3-servers/liara-docker#utilsconfigpy)
 - Added the `randomstatus` cog, which allows you to add/remove a list of statuses to the bot. Run `[p]load randomstatus` to load this cog
     - For help on how to use this command, use `[p]help status`

### Changed
 - `[p]join` and `[p]leave` now cleanup after themselves

### Fixed
 - `[p]notify` appending `None` to certain messages
