import aiohttp
import random
import discord
import asyncio
import random
from discord.ext import commands
from cogs.utils import checks
from cogs.bot.utils import p, help, config
from cogs.bot.utils.format import pagify
from cogs.utils.storage import RedisCollection

emotes = config.Load('emotes')
online_emoji = emotes.get('online')
away_emoji = emotes.get('away')
dnd_emoji = emotes.get('dnd')
offline_emoji = emotes.get('offline')
streaming_emoji = emotes.get('streaming')


class randomstatus(commands.Cog):
    """Randomly updates the bot's status message."""

    def __init__(self, liara):
        self.liara = liara
        self.db = RedisCollection(liara.redis, 'bot.penis')
        self.default_settings = {
            'statuses': [],
            'disabled': False,
            'repeat_type': 'sequential',
            'update_length': 30
        }
        self.help_set = {
            'group': 'Admin',
            'image': 'https://i.imgur.com/7i9Pm7A.png'
        }
        self.override_game = None
        self.override_stat = None
        self.status = None

    async def add_status(self,
                         game: discord.Activity,
                         status: discord.Status,
                         id: str = None):
        """Adds a status to the list of statuses.

        Parameters
        -------
        `game`: discord.Activity
            The discord.Activity object to change the status to.
        `status`: discord.Status
            The discord.Status object to be set.
        `length`: int
            How long the status should stay up for. Defaults to 30 seconds.
        `id`: str
            An optional ID to give the status.  
            Useful for when you want a cog to have its own updating status in the rotation.  
            `id` is required if you want to modify the status later on."""

        rs_settings = await self.db.get('settings', self.default_settings)
        if status == discord.Status.online:
            status = 'online'
        elif status == discord.Status.invisible:
            status = 'invisible'
        elif status == discord.Status.idle:
            status = 'idle'
        elif status == discord.Status.dnd:
            status = 'dnd'
        else:
            raise ValueError('Invalid status provided.')
        try:
            url = game.url
        except:
            url = None
        new_status = {
            'game': game.name,
            'url': url,
            'type': game.type._value_,
            'status': status,
            'id': id
        }
        if rs_settings['statuses']:
            for existing in rs_settings['statuses']:
                if existing['id'] and existing['id'] == id:
                    raise ValueError('id is already defined.')
            rs_settings['statuses'].append(new_status)
        else:
            rs_settings['statuses'] = [new_status]
        await self.db.set('settings', rs_settings)

    async def set_status(self,
                         id: str,
                         game: discord.Activity = None,
                         status: discord.Status = None):
        """Changes the status of an already defined status by id. Only statuses with ids can be edited.

        Parameters
        -------
        `id`: str
            The id of the status.
        `game`: discord.Activity
            The discord.Activity object to change the status to.
        `status`: discord.Status
            The discord.Status object to be set."""
        rs_settings = await self.db.get('settings', self.default_settings)
        new_status = None
        for n, existing in enumerate(rs_settings['statuses']):
            if existing['id'] == id:
                new_status = existing
                break
        if not new_status:
            raise ValueError('id does not exist.')
        if game:
            new_status['game'] = game.name
            new_status['url'] = game.url
            new_status['type'] = game.type
        if status:
            if status == discord.Status.online:
                new_status['status'] = 'online'
            elif status == discord.Status.invisible:
                new_status['status'] = 'invisible'
            elif status == discord.Status.idle:
                new_status['status'] = 'idle'
            elif status == discord.Status.dnd:
                new_status['status'] = 'dnd'
            else:
                raise ValueError('Invalid status provided.')
        rs_settings['statuses'][n] = new_status
        await self.db.set('settings', rs_settings)

    async def del_status(self, id: str = None, name: str = None):
        """Removes a status that either matches the ID or matches the game name.
        
        If a status with `name` appears more than once, only the first status with it
        gets deleted."""
        rs_settings = await self.db.get('settings', self.default_settings)
        if not id and not name:
            raise ValueError(
                'Come on now, you gotta specify something for me to look for.')
        if id and name:
            raise ValueError(
                'Woah hold up there, only specify either an ID or name, not both.'
            )
        resp = False
        if id:
            for n, existing in enumerate(rs_settings['statuses']):
                if existing['id'] == id:
                    del rs_settings['statuses'][n]
                    await self.db.set('settings', rs_settings)
                    resp = True
            if not resp:
                raise ValueError('That ID does not exist, you nonce.')
        if name:
            for n, existing in enumerate(rs_settings['statuses']):
                if existing['game'] == name:
                    del rs_settings['statuses'][n]
                    await self.db.set('settings', rs_settings)
                    resp = True
            if not resp:
                raise ValueError(
                    'There\'s no status with that name, you spoon.')
        if not self.override_game and not self.override_status:
            await self.liara.change_presence()
        return resp

    async def override_status(self,
                              game: discord.Activity = None,
                              status: discord.Status = None):
        """Manually override the bot's status message.
        Doing so will pause the random status function.

        To return the status back to normal, set both `game` and `status` to be a NoneType."""
        if game:
            self.override_game = game
        if status:
            self.override_stat = status
        if not game and not status:
            if self.override_game and self.override_stat:
                await self.liara.change_presence()
                self.override_game = None
                self.override_stat = None
        else:
            await self.liara.change_presence(
                activity=self.override_game, status=self.override_stat)

    @commands.group(name='status')
    @checks.is_owner()
    async def _status(self, ctx):
        """Update the status message settings."""
        if ctx.invoked_subcommand is None:
            await help.sendHelp(ctx)

    @_status.command()
    @checks.is_owner()
    async def add(self,
                  ctx,
                  status: str,
                  type: int,
                  url: str,
                  *,
                  game: str = None):
        """Add a status.
        
        `status`
        -------
        This is the online status. This can be one of the following:  
         - `online`
         - `idle`
         - `dnd`
         - `invisible`
        
        `type`
        -------
        This is the type of message to display. This can be one of the following:
         - `0` - **Playing** status
         - `1` - **Streaming** status - Requires Twitch URL as `url`
         - `2` - **Listening to** status
         - `3` - **Watching** status

        Others
        -------
        `url` only applies if `type` is `1`. Otherwise, `url` is used as the first word of `game`.
         It must be a link to a Twitch channel.
         
        `game` is the game to set the status to."""

        statuses = ['online', 'idle', 'dnd', 'invisible']
        types = [0, 1, 2, 3]
        type_str = ['Playing', 'Streaming', 'Listening to', 'Watching']
        if status not in statuses:
            return await help.sendHelp(ctx)
        if type not in types:
            return await help.sendHelp(ctx)

        if status == 'online':
            status = discord.Status.online
            emoji = online_emoji
        elif status == 'invisible':
            status = discord.Status.invisible
            emoji = offline_emoji
        elif status == 'idle':
            status = discord.Status.idle
            emoji = away_emoji
        elif status == 'dnd':
            status = discord.Status.dnd
            emoji = dnd_emoji
        else:
            # This should never happen but oh well lol
            return await help.sendHelp(ctx)

        if type == 1:
            if not 'twitch.tv' in url:
                return await help.sendHelp(ctx)
            emoji = streaming_emoji
        else:
            if game:
                game = '{} {}'.format(url, game)
            else:
                game = url
            url = None
        game_obj = discord.Activity(name=game, url=url, type=type)
        await self.add_status(game_obj, status)
        await ctx.send('Added status: {}**{}** {}{}'.format(
            emoji, type_str[type], game, ' ({})'.format(url) if url else ''))

    @_status.command()
    @checks.is_owner()
    async def remove(self, ctx, *, game: str):
        """Remove a status based on its game."""
        try:
            await self.del_status(name=game)
            await ctx.send('Status removed.')
        except ValueError as e:
            return await ctx.send(e)

    @_status.command()
    @checks.is_owner()
    async def list(self, ctx):
        """List all statuses."""
        rs_settings = await self.db.get('settings', self.default_settings)
        statuses = rs_settings['statuses']
        types = [0, 1, 2, 3]
        type_str = ['Playing', 'Streaming', 'Listening to', 'Watching']

        status_list = []
        for status in statuses:
            if status['status'] == 'online':
                emoji = online_emoji
            elif status['status'] == 'invisible':
                emoji = offline_emoji
            elif status['status'] == 'idle':
                emoji = away_emoji
            elif status['status'] == 'dnd':
                emoji = dnd_emoji

            if status['type'] == 1:
                emoji = streaming_emoji

            status_str = '{}**{}** {}{}{}'.format(
                emoji, type_str[status['type']], status['game'],
                ' (<{}>)'.format(status['url']) if status['url'] else '',
                ' *(ID: {})*'.format(status['id']) if status['id'] else '')
            status_list.append(status_str)

        if not status_list:
            return await ctx.send('No custom statuses set!')
        message = 'Custom statuses on rotation:\n\n'
        message += '\n'.join(status_list)
        for page in pagify(message, delims=[' ', '\n'], shorten_by=8):
            await ctx.send(page)

    @_status.command()
    @checks.is_owner()
    async def shuffle(self, ctx):
        """Shuffle the status rotation randomly."""
        rs_settings = await self.db.get('settings', self.default_settings)

        if not rs_settings['statuses']:
            return await ctx.send('There are no custom statuses set!')

        random.shuffle(rs_settings['statuses'])

        await self.db.set('settings', rs_settings)

        await ctx.send('Status list shuffled!')

    @_status.command()
    @checks.is_owner()
    async def length(self, ctx, seconds: int):
        """Change how frequently the status updates.
        
        Minimum length of time is 30 seconds."""

        if seconds < 30:
            return await help.sendHelp(ctx)

        rs_settings = await self.db.get('settings', self.default_settings)
        rs_settings['update_length'] = seconds
        await self.db.set('settings', rs_settings)

        await ctx.send(
            'The status will now update every {} seconds.'.format(seconds))

    async def random_status(self):
        while not self.liara.is_ready():
            await asyncio.sleep(0.1)
        while self is self.liara.get_cog('randomstatus'):
            if not self.override_game or not self.override_stat:
                rs_settings = await self.db.get('settings',
                                                self.default_settings)
                if rs_settings['statuses']:
                    for status in rs_settings['statuses']:
                        if self.override_game or self.override_stat:
                            break
                        if status['status'] == 'online':
                            status_obj = discord.Status.online
                        elif status['status'] == 'invisible':
                            status_obj = discord.Status.invisible
                        elif status['status'] == 'idle':
                            status_obj = discord.Status.idle
                        elif status['status'] == 'dnd':
                            status_obj = discord.Status.dnd

                        game_obj = discord.Activity(
                            name=status['game'],
                            url=status['url'],
                            type=status['type'])
                        for attempt in range(10):
                            try:
                                self.status = status_obj
                                await self.liara.change_presence(
                                    activity=game_obj, status=status_obj)
                                break
                            except Exception as e:
                                await asyncio.sleep(10)
                                if attempt == 10:
                                    print(
                                        '[RandomStatus] | Failed to set status after 10 attempts: {}'
                                        .format(str(e)))
                        await asyncio.sleep(rs_settings['update_length'])
                elif self.status:
                    asyncio.sleep(120)
                    for attempt in range(10):
                        try:
                            await self.liara.change_presence()
                        except:
                            await asyncio.sleep(10)
                else:
                    await asyncio.sleep(1)
            else:
                await asyncio.sleep(1)
        await self.liara.change_presence()


def setup(liara):
    cog = randomstatus(liara)
    liara.add_cog(cog)
    liara.loop.create_task(cog.random_status())
