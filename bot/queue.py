import discord
import asyncio
from discord.ext import commands
from cogs.utils import checks
from cogs.utils.storage import RedisCollection
from cogs.bot.utils import help


class Queue(commands.Cog):
    """Manage/join user queues."""

    def __init__(self, liara):
        self.liara = liara
        self.help_set = {
            'group': 'General',
            'image': 'https://i.imgur.com/WQLeH4z.png'
        }
        self.db = RedisCollection(liara.redis, 'bot.queue')

    async def role_permissions_check(self, ctx):
        """Checks to make sure the bot has the needed permissions to edit roles."""
        bot = ctx.guild.me
        if not bot.guild_permissions.manage_roles:
            return False
        return True

    async def channel_permissions_check(self, ctx,
                                        channel: discord.TextChannel):
        """Checks to make sure the bot has the needed permissions for the channel."""
        bot = ctx.guild.me
        perms = bot.permissions_in(channel)
        missing_perms = []
        if not perms.read_messages:
            missing_perms.append('Read Messages')
        if not perms.read_message_history:
            missing_perms.append('Read Message History')
        if not perms.send_messages:
            missing_perms.append('Send Messages')
        if not perms.manage_messages:
            missing_perms.append('Manage Messages')
        return missing_perms

    async def role_hirearchy_check(self, ctx, role: discord.Role):
        """Checks to make sure the bot is above the role so it can add users to it."""
        bot = ctx.guild.me
        if role >= bot.roles[-1]:
            return False
        return True

    async def construct_queue(self, queue: dict):
        """Returns a formatted list of strings to be used in a series of messages."""
        name = queue.get('name', '')
        users = queue.get('users', [])
        limit = queue.get('limit', 0)
        author = queue.get('author', None)
        if author:
            created = '| **Created by:** {} '.format(author['name'])
        else:
            created = ''
        header = '**Queue:** {} {}| {}{}{} users:\n'.format(
            name, created, len(users), '/' if limit else '',
            str(limit) if limit else '')

        lines = [header]

        for i, user in enumerate(users):
            pos = i + 1
            line = '**{}.** {} *({})*'.format(pos, user['name'], user['id'])
            lines.append(line)

        if not users:
            lines.append('*There are no users in this queue.*')

        messages = []
        message = ''

        for line in lines:
            total_len = len(message) + len(line)
            if total_len > 1800:
                messages.append(message)
                message = ''
            message += '\n{}'.format(line)

        messages.append(message)

        return messages

    async def update_queue(self, guild: discord.Guild, queue_pos: int):
        """Updates a queue's messages."""
        queues = await self.db.get(guild.id, [])
        queue = queues[queue_pos]
        limit = queue.get('limit', 0)
        messages = await self.construct_queue(queue)

        existing = []

        channel = guild.get_channel(queue['channel'])

        for msg in queue['messages']:
            existing.append(await channel.fetch_message(msg))

        for m, e in zip(list(messages), list(existing)):
            await e.edit(content=m)
            messages.remove(m)
            existing.remove(e)

        for e in existing:
            queue['messages'].remove(e.id)

        for m in messages:
            msg = await channel.send(m)
            queue['messages'].append(msg.id)

        queues[queue_pos] = queue

        await self.db.set(guild.id, queues)

    @commands.command('create')
    @commands.guild_only()
    @checks.admin_or_permissions(manage_roles=True)
    async def create(self,
                     ctx,
                     channel: discord.TextChannel,
                     name: str,
                     limit: str = '0',
                     *,
                     role: str = ''):
        """Creates a new queue for users to join.

        The queue name should be in double quotes if it contains spaces.

        - `channel` - The channel used to store the queue
        - `name` - The name of the queue people use to join with
        - `limit` - The maximum number of people allowed in a queue (Optional)
        - `role` - The role assigned to users upon joining the queue (Optional).

        Note that deleting the channel will also delete the queue. 

        The queue name and role name should both be surrounded in double quotes if they contain a space.

        If a limit is specified, then only that many people are allowed to join. A limit of 16 is imposed on any queues that share a channel.

        If `limit` is not a number, then it is used as the first word of the role name.

        If a role is specified, then users will be assigned the role upon joining the queue.

        Examples
        -------
        `[p]create #channel "Queue Name" 10 Role Name`
        `[p]create #channel "Queue Name" Role Name` 
        `[p]create #channel "Queue Name"`"""

        try:
            limit = int(limit)
        except:
            if limit and isinstance(limit, str):
                role = '{}{}{}'.format(limit, ' ' if role else '', role)
                limit = 0

        # Set the role variable and check permissions
        if role:
            role = discord.utils.get(ctx.guild.roles, name=role)
            if not role:
                return await ctx.send('That role doesn\'t exist.')
            if not await self.role_permissions_check(ctx):
                return await ctx.send(
                    'I do not have the **Manage Roles** permission needed to add users to the role.'
                )
            if not await self.role_hirearchy_check(ctx, role):
                return await ctx.send(
                    'That role is too high in the hirearchy for me to edit.')
        perms = await self.channel_permissions_check(ctx, channel)
        if perms:
            perms_list = " and ".join([", ".join(perms[:-1]), perms[-1]
                                      ]) if len(perms) > 1 else perms[0]
            return await ctx.send(
                'I do not have the **{}** permission{} for {}.'.format(
                    perms_list, 's' if len(perms) > 1 else '', channel.mention))

        queues = await self.db.get(ctx.guild.id, [])

        # Check for any duplicate information in the database
        update_queues = []
        for p, q in enumerate(queues):
            if q['name'].lower() == name.lower():
                return await ctx.send(
                    'A queue named {} already exists.'.format(name))
            if q['channel'] == channel.id:
                if len(q['users']) > 16:
                    return await ctx.send(
                        'Channel {} already has a queue ({}) with more than 16 people in.'
                        .format(channel.mention, q['name']))
                else:
                    limit = 16 if not limit else limit
                    q_limit = q.get('limit', 0)
                    if not q_limit or q_limit > 16:
                        queues[p]['limit'] = 16
                        update_queues.append(p)
            if role:
                if q['role'] == role.id:
                    return await ctx.send(
                        'Role {} is already used for queue {}.'.format(
                            role.name, q['name']))

        # Construct the dictionary
        d = {
            'name': name,
            'channel': channel.id,
            'role': role.id if role else None,
            'messages': [],
            'users': [],
            'limit': limit,
            'author': {
                'name': ctx.author.display_name,
                'id': ctx.author.id
            }
        }

        strings = await self.construct_queue(d)

        messages = []

        for s in strings:
            msg = await channel.send(s)
            messages.append(msg.id)

        d['messages'] = messages

        # Add to the database
        queues.append(d)
        await self.db.set(ctx.guild.id, queues)
        for i in update_queues:
            await self.update_queue(ctx.guild, i)

        await ctx.send(
            'Queue {} created. Join the queue using `{}join {}`.'.format(
                name, self.liara.command_prefix[0], name.lower()))

    @commands.command('delete')
    @commands.guild_only()
    @checks.admin_or_permissions(manage_roles=True)
    async def delete(self, ctx, *, name: str):
        """Deletes a queue. Do note that the bot will not remove people from any roles assigned by this queue.

        - 'name' - The name of the queue to delete.
        
        Any role or channel assigned to this queue will not be removed, but any message associated with the queue will be."""
        queues = await self.db.get(ctx.guild.id, [])

        if not queues:
            return await ctx.send('There are no queues on this server.')

        for q in list(queues):
            if q['name'].lower() == name.lower():
                # Delete all messages associated with the queue.
                channel = ctx.guild.get_channel(q['channel'])
                for msg in q['messages']:
                    try:
                        msg = await channel.fetch_message(msg)
                        await msg.delete()
                    except:
                        pass
                queues.remove(q)
                await self.db.set(ctx.guild.id, queues)
                return await ctx.send('The queue has been removed successfully.'
                                     )
        return await ctx.send(
            'There is no queue called {}. Send `{}list` for a list of queues.'.
            format(name, self.liara.command_prefix[0]))

    @commands.command('notify')
    @commands.guild_only()
    @checks.admin_or_permissions(manage_roles=True)
    async def notify(self, ctx, queue: str, number: str, message: str = None):
        """DMs a certain amount of users in a queue with a specified message.

        - `queue` - The queue to message (Must be in quotes)
        - `number` - The number of people to notify (Optional)
        - `message` - The message to send to users.

        If `number` isn't a number, then it is used as the first word of `message` and all queue members will be messaged.
        
        Examples
        -------
        `[p]notify "Queue Name" 5 Hello, world!`
        `[p]notify "Queue name" Hello, world!`"""
        try:
            number = int(number)
        except:
            message = f'{number}{" " if message else ""}{message if message else ""}'
            number = 0

        if not message:
            return await help.sendHelp(ctx)

        queues = await self.db.get(ctx.guild.id, [])

        for q in queues:
            if q['name'].lower() == queue.lower():
                if not q['users']:
                    return await ctx.send('There are no users in this queue.')
                author = ctx.author
                server = ctx.guild
                if isinstance(author, discord.Member):
                    colour = author.colour
                else:
                    colour = discord.Colour.red()
                e = discord.Embed(colour=colour, description=message)

                author_desc = 'Message from {}'.format(str(author))
                if author.avatar_url:
                    e.set_author(name=author_desc, icon_url=author.avatar_url)
                else:
                    e.set_author(name=author_desc)
                e.set_footer(text='{} | {}'.format(server.name, q['name']))

                if number:
                    users = q['users'][:number]
                else:
                    users = q['users']
                failed = []

                for user in users:
                    try:
                        u = server.get_member(user['id'])
                        await u.send(embed=e)
                    except:
                        failed.append(user['name'])

                if failed:
                    return await ctx.send(
                        'The message failed to send to the following users:\n\n{}'
                        .format(', '.join(failed)))
                else:
                    return await ctx.send(
                        'Message sent to {} queue members.'.format(
                            'the first {}'.format(str(number))
                            if number and number < len(q['users']) else 'all'))
        return await ctx.send('There is no queue by that name.')

    @commands.command('add')
    @commands.guild_only()
    @checks.admin_or_permissions(manage_roles=True)
    async def add_user(self, ctx, user: discord.Member, *, queue: str = ''):
        """Add a user to a queue.

        - `user` - The user you want to add
        - `queue` - The queue to add the user to (Optional*).

        If using the user's name, then be sure to enclose it in double quotes.

        *Only optional if running in a channel with one queue in it.
        
        Examples
        -------
        `[p]add 155787231968100352 Queue Name`
        `[p]add @Sloth Queue Name`
        `[p]add "Bob Ross" Queue Name`
        `[p]add Dave`"""
        queues = await self.db.get(ctx.guild.id, [])

        # # Let's keep this channel clean
        try:
            await ctx.message.delete()
        except:
            pass

        if not queues:
            return await ctx.send(
                'There are no queues on this server.', delete_after=5)

        if len([q for q in queues if q['channel'] == ctx.channel.id]) > 1:
            multi = True
        else:
            multi = False

        for i, q in enumerate(queues):
            if (queue.lower() == q['name'].lower()) is not (
                    not queue and q['channel'] == ctx.channel.id):
                if multi and not queue:
                    return await ctx.send(
                        'Please specify a queue to modify, there\'s more than one queue in this channel.',
                        delete_after=5)
                channel = ctx.guild.get_channel(q['channel'])
                if not channel:
                    # This error message should never appear.
                    return await ctx.send(
                        '🤔 Hmmm, it looks like this queue\'s channel has been deleted.',
                        delete_after=5)

                if await self.channel_permissions_check(ctx, channel):
                    # Kinda pointless to send this in a channel it can't really use but oh well
                    return await ctx.send(
                        '🤔 Hmmm, it looks like my channel permissions aren\'t setup correctly.',
                        delete_after=5)

                if q['role']:
                    role = discord.utils.get(ctx.guild.roles, id=q['role'])
                    if not await self.role_permissions_check(ctx):
                        return await ctx.send(
                            '🤔 Hmmm, it looks like my role permissions aren\'t setup correctly.',
                            delete_after=5)
                    if not role:
                        return await ctx.send(
                            '🤔 Hmmm, it looks like this queue\'s role has been deleted.',
                            delete_after=5)
                    if not await self.role_hirearchy_check(ctx, role):
                        return await ctx.send(
                            '🤔 Hmmm, it looks like my role permissions aren\'t setup correctly.',
                            delete_after=5)

                for u in q['users']:
                    if user.id == u['id']:
                        return await ctx.send(
                            '{} That user is already in the queue.'.format(
                                ctx.author.mention),
                            delete_after=5)

                limit = q.get('limit', 0)
                if limit and len(q['users']) + 1 > limit:
                    return await ctx.send(
                        'Looks like that queue\'s full, try again when there\'s space.',
                        delete_after=5)

                queues[i]['users'].append({'name': str(user), 'id': user.id})
                await self.db.set(ctx.guild.id, queues)
                await self.update_queue(ctx.guild, i)
                if q['role']:
                    await ctx.author.add_roles(
                        role, reason='Joining {} queue.'.format(q['name']))
                return await ctx.send(
                    '{} That user has been added to this queue.'.format(
                        ctx.author.mention),
                    delete_after=5)
        if not queue:
            response = 'There\'s no queue in this channel.'
        else:
            response = 'There\'s no queue with that name.'
        return await ctx.send(
            '{} {}'.format(ctx.author.mention, response), delete_after=5)

    @commands.command('remove')
    @commands.guild_only()
    @checks.admin_or_permissions(manage_roles=True)
    async def remove_user(self, ctx, user: int, *, queue: str = ''):
        """Removes a user from a queue.

        - `user` - The user you want to remove. This can be either the user's ID or the user's position in the channel.
        - `queue` - The queue you want to remove them from.
        
        Examples
        -------
        `[p]remove 155787231968100352 Queue Name`
        `[p]remove 3 Queue Name`"""
        queues = await self.db.get(ctx.guild.id, [])

        # # Let's keep this channel clean
        try:
            await ctx.message.delete()
        except:
            pass

        if not queues:
            return await ctx.send(
                'There are no queues on this server.', delete_after=5)

        member = ctx.guild.get_member(user)

        if len([q for q in queues if q['channel'] == ctx.channel.id]) > 1:
            multi = True
        else:
            multi = False

        for i, q in enumerate(queues):
            if (queue.lower() == q['name'].lower()) is not (
                    not queue and q['channel'] == ctx.channel.id):
                if multi and not queue:
                    return await ctx.send(
                        'Please specify a queue to modify, there\'s more than one queue in this channel.',
                        delete_after=5)
                channel = ctx.guild.get_channel(q['channel'])
                if not channel:
                    # This error message should never appear.
                    return await ctx.send(
                        '🤔 Hmmm, it looks like this queue\'s channel has been deleted.',
                        delete_after=5)

                if await self.channel_permissions_check(ctx, channel):
                    # Kinda pointless to send this in a channel the bot can't really use but oh well
                    return await ctx.send(
                        '🤔 Hmmm, it looks like my channel permissions aren\'t setup correctly.',
                        delete_after=5)

                if q['role']:
                    role = discord.utils.get(ctx.guild.roles, id=q['role'])
                    if not await self.role_permissions_check(ctx):
                        return await ctx.send(
                            '🤔 Hmmm, it looks like my role permissions aren\'t setup correctly.',
                            delete_after=5)
                    if not role:
                        return await ctx.send(
                            '🤔 Hmmm, it looks like this queue\'s role has been deleted.',
                            delete_after=5)
                    if not await self.role_hirearchy_check(ctx, role):
                        return await ctx.send(
                            '🤔 Hmmm, it looks like my role permissions aren\'t setup correctly.',
                            delete_after=5)

                if member:
                    for pos, u in enumerate(list(q['users'])):
                        if member.id == u['id']:
                            del queues[i]['users'][pos]
                            await self.db.set(ctx.guild.id, queues)
                            await self.update_queue(ctx.guild, i)
                            if q['role']:
                                try:
                                    await member.remove_roles(
                                        role,
                                        reason='Leaving {} queue.'.format(
                                            q['name']))
                                except:
                                    pass
                            return await ctx.send(
                                '{} {} has been removed from this queue.'.
                                format(ctx.author.mention, str(member)),
                                delete_after=5)
                    return await ctx.send(
                        '{} is not in the queue'.format(str(member)),
                        delete_after=5)
                else:
                    try:
                        d = queues[i]['users'][user - 1]
                        del queues[i]['users'][user - 1]
                        await self.db.set(ctx.guild.id, queues)
                        await self.update_queue(ctx.guild, i)
                        member = ctx.guild.get_member(d['id'])
                        if member and q['role']:
                            try:
                                await member.remove_roles(
                                    role,
                                    reason='Leaving {} queue.'.format(
                                        q['name']))
                            except:
                                pass
                        return await ctx.send(
                            '{} {} has been removed from this queue.'.format(
                                ctx.author.mention,
                                str(member) if member else 'That user'),
                            delete_after=5)
                    except IndexError:
                        return await ctx.send(
                            '{} That is not a valid user ID or list position.'.
                            format(ctx.author.mention),
                            delete_after=5)
        if not queue:
            response = 'There\'s no queue in this channel.'
        else:
            response = 'There\'s no queue with that name.'
        return await ctx.send(
            '{} {}'.format(ctx.author.mention, response), delete_after=5)

    @commands.command('join')
    @commands.guild_only()
    async def join(self, ctx, *, name: str):
        """Joins a queue.

        - `name` - The name of the queue to join."""
        author = ctx.author
        mention = author.mention
        queues = await self.db.get(ctx.guild.id, [])

        try:
            await ctx.message.delete()
        except:
            pass

        if not queues:
            return await ctx.send('There are no queues on this server.', delete_after=5)

        if not await self.role_permissions_check(ctx):
            return await ctx.send(
                f'🤔 {mention} Hmmm, it looks like my role permissions aren\'t setup correctly.',
                delete_after=5
            )

        for i, q in enumerate(queues):
            if name.lower() == q['name'].lower():
                channel = ctx.guild.get_channel(q['channel'])
                if not channel:
                    # This error message should never appear.
                    return await ctx.send(
                        f'🤔 {mention} Hmmm, it looks like that queue\'s channel has been deleted.',
                        delete_after=5
                    )

                if await self.channel_permissions_check(ctx, channel):
                    return await ctx.send(
                        f'🤔 {mention} Hmmm, it looks like my channel permissions aren\'t setup correctly.',
                        delete_after=5
                    )

                if q['role']:
                    role = discord.utils.get(ctx.guild.roles, id=q['role'])
                    if not role:
                        return await ctx.send(
                            f'🤔 {mention} Hmmm, it looks like that queue\'s role has been deleted.',
                            delete_after=5
                        )
                    if not await self.role_hirearchy_check(ctx, role):
                        return await ctx.send(
                            f'🤔 {mention} Hmmm, it looks like my role permissions aren\'t setup correctly.',
                            delete_after=5
                        )

                for user in q['users']:
                    if author.id == user['id']:
                        return await ctx.send(
                            f'{mention} You are already in this queue. You can leave by sending `{self.liara.command_prefix[0]}leave {q["name"].lower()}`.',
                            delete_after=5)
                limit = q.get('limit', 0)
                if limit and len(q['users']) + 1 > limit:
                    return await ctx.send(
                        f'{mention} Looks like that queue\'s full, try again when there\'s space.',
                        delete_after=5
                    )

                queues[i]['users'].append({
                    'name': str(author),
                    'id': author.id
                })
                await self.db.set(ctx.guild.id, queues)
                await self.update_queue(ctx.guild, i)
                if q['role']:
                    await ctx.author.add_roles(
                        role, reason=f'Joining {q["name"]} queue.')
                return await ctx.send(
                    f'{mention} I\'ve added you to the {q["name"]} queue. You can leave by sending `{self.liara.command_prefix[0]}leave {q["name"].lower()}`.',
                    delete_after=5)
        return await ctx.send(f'{mention} There\'s no queue named {name}.', delete_after=5)

    @commands.command('leave')
    @commands.guild_only()
    async def leave(self, ctx, *, name: str):
        """Leaves a queue.

        - `name` - The name of the queue to leave."""
        author = ctx.author
        mention = author.mention
        queues = await self.db.get(ctx.guild.id, [])

        try:
            await ctx.message.delete()
        except:
            pass

        if not queues:
            return await ctx.send(f'{mention} There are no queues on this server.', delete_after=5)

        if not await self.role_permissions_check(ctx):
            return await ctx.send(
                f'🤔 {mention} Hmmm, it looks like my role permissions aren\'t setup correctly.',
                delete_after=5
            )

        for i, q in enumerate(queues):
            if name.lower() == q['name'].lower():
                channel = ctx.guild.get_channel(q['channel'])
                if not channel:
                    # This error message should never appear.
                    return await ctx.send(
                        f'🤔 {mention} Hmmm, it looks like that queue\'s channel has been deleted.',
                        delete_after=5
                    )
                if await self.channel_permissions_check(ctx, channel):
                    return await ctx.send(
                        f'🤔 {mention} Hmmm, it looks like my channel permissions aren\'t setup correctly.',
                        delete_after=5
                    )

                if q['role']:
                    role = discord.utils.get(ctx.guild.roles, id=q['role'])
                    if not role:
                        return await ctx.send(
                            f'🤔 {mention} Hmmm, it looks like that queue\'s role has been deleted.',
                            delete_after=5
                        )
                    if not await self.role_hirearchy_check(ctx, role):
                        return await ctx.send(
                            f'🤔 {mention} Hmmm, it looks like my role permissions aren\'t setup correctly.',
                            delete_after=5
                        )

                for pos, user in enumerate(list(q['users'])):
                    if author.id == user['id']:
                        del queues[i]['users'][pos]
                        await self.db.set(ctx.guild.id, queues)
                        await self.update_queue(ctx.guild, i)
                        if q['role']:
                            try:
                                await ctx.author.remove_roles(
                                    role,
                                    reason=f'Leaving {q["name"]} queue.')
                            except:
                                pass
                        return await ctx.send(
                            f'{mention} You have left the {q["name"]} queue.',
                            delete_after=5)

                return await ctx.send(f'{mention} You\'re not in the {q["name"]} queue.',
                    delete_after=5)
        return await ctx.send(f'{mention} There\'s no queue named {name}.', delete_after=5)

    @commands.command('info')
    async def queue_info(self, ctx, *, queue: str = None):
        """List information about the specified queue."""
        queues = await self.db.get(ctx.guild.id, [])
        if not queues:
            return await ctx.send('There are no queues on this server.')
        if not queue:
            if len(queues) == 1:
                final_queue = queue[0]
            else:
                return await ctx.send(
                    'Please specify a queue to view, do `{}list` to list all queues.'
                    .format(self.liara.command_prefix[0]))
        else:
            for i, q in enumerate(queues):
                if queue.lower() == q['name'].lower():
                    final_queue = q

        if not final_queue:
            return await ctx.send(
                'There is no queue by that name, do `{}list` to list all queues.'
                .format(self.liara.command_prefix[0]))

        messages = await self.construct_queue(final_queue)

        for message in messages:
            await ctx.send(message)

    @commands.command('list')
    @commands.guild_only()
    async def queue_list(self, ctx):
        """Lists all the available queues on this server."""
        queues = await self.db.get(ctx.guild.id, [])

        if not queues:
            return await ctx.send('There are no queues on this server.')

        lines = []

        lines.append('Here is a list of queues on this server:\n')

        for q in queues:
            length = len(q['users'])
            limit = q.get('limit', 0)
            s = " - **{}** ({}{}{} user{})".format(
                q['name'], str(length), '/' if limit else '',
                str(limit) if limit else '', 's' if limit != 1 else '')
            lines.append(s)

        lines.append(
            '\nTo join a queue, send `{}join` followed by the queue name.'.
            format(self.liara.command_prefix[0]))

        message = ''
        messages = []

        for line in lines:
            total_len = len(message) + len(line)
            if total_len > 1800:
                messages.append(message)
                message = ''
            message += '\n{}'.format(line)
        messages.append(message)

        for msg in messages:
            await ctx.send(msg)

    async def on_member_remove(self, member: discord.Member):
        guild = member.guild
        queues = await self.db.get(guild.id, [])
        for i, q in enumerate(queues):
            for pos, u in enumerate(q['users']):
                if u['id'] == member.id:
                    del queues[i]['users'][pos]
                    await self.db.set(guild.id, queues)
                    await self.update_queue(guild, i)

    async def on_guild_channel_delete(self, channel):
        guild = channel.guild
        queues = await self.db.get(guild.id, [])
        for q in list(queues):
            if q['channel'] == channel.id:
                queues.remove(q)
                await self.db.set(guild.id, queues)

    async def on_member_update(self, before, after):
        if before.display_name != after.display_name:
            guild = before.guild
            queues = await self.db.get(guild.id, [])
            for p, queue in enumerate(queues):
                author = queue.get('author', {'name': None, 'id': None})
                if author['id'] == after.id:
                    queues[p]['author']['name'] = after.display_name
                    await self.db.set(guild.id, queues)
                    await self.update_queue(guild, p)


def setup(liara):
    liara.add_cog(Queue(liara))
